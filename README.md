# Odtwarzacz MP3

## Description
Odtwarzacz mp3 zdolny jest do dodawania piosenek, przewijania, usuwania ich, oraz zmiany poziomu głośności. Wszystkie linijki kodu są podpisane poprzedzającym #, który traktuje o tym czego dotyczą.

## Installation
Program został napisany przy użyciu języka python z wykorzystaniem tkinter oraz pygame. By go uruchomić należy utworzyć folder "py' na dysku C, wewnątrz należy utworzyć 2 foldery, "audio" oraz "images", do folederu audio należy dodać wybrane przez siebie pliki mp3, które chcemy odtworzyć. Do folderu images należy dodać 5 plików graficznych udostępnionych wyżej, które stanowią przyciski odtwarzacza. Na koniec przenieść "player.py" do folderu "py". Aby uruchomić program nalezy wpisać komendę "python player.py" w terminalu. By go uruchomić możliwe iż będzie konieczne wczesniejsze wpisanie zmiany lokalizacji wyszukiwania plików w terminalu, zrobić to można komendą "cd /c/py". 

## Authors and acknowledgment
Projekt został wykonany przy pomocy serii poradników od codemy.com.
